import React, { Component, useState } from 'react'
import { Text, StyleSheet, View, SafeAreaView, TextInput, Button } from 'react-native'
import database from '@react-native-firebase/database';

// const firebaseConfig = {
//   apiKey: "AIzaSyAbBHMLLz5a1xAyzqTjtqv6q8dV992G3w0",
//   authDomain: "test-realtimedb-e2925.firebaseapp.com",
//   databaseURL: "https://test-realtimedb-e2925.firebaseio.com",
//   projectId: "test-realtimedb-e2925",
//   storageBucket: "test-realtimedb-e2925.appspot.com",
//   messagingSenderId: "123016388902",
//   appId: "1:123016388902:web:9187ff378b2acc1551a1e7",
//   measurementId: "G-6R8RBF8VBB"
// };

// Initialize Firebase
// if(!firebase.apps.length){
//   firebase.initializeApp(firebaseConfig);
// }

export default class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
       name : "",
       age : ""
    }
  }


  handlerSubmit = () =>{
    database()
    .ref('/users')
    .set({
      name: this.state.name,
      age: this.state.age
    })
    .then(() => console.log('Data set.'));
  }

  handlerReadData = () => {
    database()
  .ref('/users')
  .on('value', snapshot => {
    console.log('User data: ', snapshot.val());
  });
  }
  
  
  handlerUpdateData = () =>{
      database()
    .ref('/users')
    .update({
      name : 'new update name',
      age: 32,
    })
    .then(() => console.log('Data updated.'));
  }

  handlerDeleteData = async () =>{
    await database()
    .ref('/users')
    .remove()
    .set(this.setState({
      name : '',
      age : ''
    }));
}

  render() {
    return (
      <SafeAreaView>
        <Text> Real time db testing</Text>
        <Text>Enter Username : </Text>
        <TextInput
          placeholder = "Enter name"
          onChangeText = {(name) => this.setState({name})}
          value = {this.state.name}
        />

        <Text>Enter Age : </Text>
        <TextInput
          placeholder = "Enter age"
          onChangeText = {(age) => this.setState({age})}
          value = {this.state.age}
        />

        <Button 
          title = "Submit"
          onPress = {()=>this.handlerSubmit()}
        />

        <Button 
          title = "Read Data"
          onPress = {()=>this.handlerReadData()}
        />

        <Button 
          title = "Update Data"
          onPress = {()=>this.handlerUpdateData()}
        />

        <Button 
          title = "Delete Data"
          onPress = {()=>this.handlerDeleteData()}
        />

      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({})
